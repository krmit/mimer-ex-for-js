# Exempel

Denna mapp innehåller ett antal exempel baserat på kapitlet 
"JS och funktioner", som du hittar 
[här](https://htsit.se/a/topics/programming/js/This-and-prototypes/index.html). 

Observera att kapitlet inte är uppdaterat för tillfället och därför 
stämmer inte numreringen så bra.

Men annars ska namnet på exemplerna är numret på avsnittet i kapitlet 
som exemplet berör, följd av ett tal som anger vilken ordning som exemplet 
har. De enklaste exemplerna kommer i oftast först.


## Övning

För att öva dig utifrån dessa exempel:

  * Testa att köra exemplerna.
  * Läs igenom koden och testa den, så att du förstår vad som händer.
  * Ändra något i koden och se vd som händer.
  * Ta bort all kod och se om du kan lösa uppgiften på ett eget sätt.
  * Skapa egna varianter av exemplet eller helt nya exempel och skicka in dem.
  
Kommentar och nya exempel skicka till magnus.kronnas@hassleholm.se
