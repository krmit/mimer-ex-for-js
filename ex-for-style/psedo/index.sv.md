# Om psedo i CSS

Detta är en samling av exempel om psedo klasser och element

## Psedo

   * [Psedo element](./psedo--element.html)
   * [Psedo class](./psedo--classes.html)
   * [Länkar med psedo](./link-with-psedo.html)
   * [Selekt med psedo](./select-with-psedo.html)
