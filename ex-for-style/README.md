# Exempel

Denna mapp innehåller ett antal exempel baserat på kapitlet 
"Stil med CSS", som du hittar 
[här](https://docs.google.com/document/d/1g00t32iPU7LJ92MdjGdtKT4VMtz4_JGtHCrfEFgE6i0/edit?usp=sharing). 

Namnet på exemplerna är numret på avsnittet i kapitlet som exemplet berör, 
följd av ett tal som anger vilken ordning som exemplet har. De enklaste 
exemplerna kommer i oftast först. Om talet följa av ett "!" så är det att 
betrakta som extra svårt. 


## Övning

För att öva dig utifrån dessa exempel:

  * Testa undersöka exemplerna.
  * Läs igenom koden och testa dem, så att du förstår vad som händer.
  * Ändra något i koden och se vad som händer.
  * Skapa egna varianter av exemplet eller helt nya exempel och skicka in dem.
  
Kommentar och nya exempel skicka till magnus.kronnas@hassleholm.se
