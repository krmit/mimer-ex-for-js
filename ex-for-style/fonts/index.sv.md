# Om fonter i CSS

Detta är en samling av exempel om fonter

OBS! Vi diskuterar inte webbfonter i denna kurs,men det kan vara värt att undersöka om du vill använda olika fonter på webben.

## Fonter

   * [Enkelt exempel](./fonts.html)
   * [Fonts alternativ](./fonts-alternative.html)
   * [Mer om fonter](./more-fonts.html)
