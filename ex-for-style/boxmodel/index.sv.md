# Om boxmodelen i CSS

Detta är en samling av exempel om boxmodelen

## Färger

   * [Enkelt exempel](./boxmodelen.html)
   * [Padding](./padding.html)
   * [Border size](./border-size.html)
   * [Margin](./margin.html)
   * [Mix](./mix.html)
