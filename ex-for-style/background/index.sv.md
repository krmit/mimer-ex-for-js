# Om backgrund i CSS

Detta är en samling av exempel om backgrund

## Backgrund

   * [Enkelt exempel](./background.html)
   * [Backgrunds bild](./background-image.html)
   * [Repetera backgrund](./repeat.html)
   * [Inte repetera](./no-repeat.html)
   * [Repetera horizontelt](./repeat-horizontally.html)
   * [Repetera verticalt](./repeat-vertical.html)
   * [Posistion på background](./background-position.html)
   * [Mer om Posistion på background](./background-position-1.html)
