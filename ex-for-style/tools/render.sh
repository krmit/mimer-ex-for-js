#!/bin/bash

find * -type d  \! -path \*/\.git\* \! -path \*tools\* -exec bash -c "echo -e '\e[32m{}'; cd {};pwd;ghmd *.md;ghmd *.sv.md;" \;
echo "Refactor";
ghmd index.sv.md;
mv index.sv.html index.html
echo "Uppload to server.";
rsync -arvz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress */ index.html htsit.se:~/www/bb/
