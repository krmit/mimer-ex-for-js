# Om selektor i CSS

Detta är en samling av exempel om selektorer

## Selektor

   * [Enkelt exempel](./selector.html)
   * [Select a tag](./select-tag.html)
   * [Select a tagar](./selector-tag.html)
   * [Select med tag och id](./select-tag-and-id.html)
   * [Select med tag och klass](./select-tag-and-class.html)
   * [Select med id och klass](./select-id-and-class.html)
