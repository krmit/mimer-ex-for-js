# Om kantlinjer i CSS

Detta är en samling av exempel om kantlinjer

## Kanter

   * [border](./border.html)
   * [Bred på kantlinjen](./border-widht.html)
   * [Kantlinjer kring rubrik](./header-border.html)
   * [Färg på kantlinjen](./border-color.html)
   * [Prickad kantlinje](./dotted.html)
   * [Fler kantlinjer](./more-border-style.html)
   * [Runda kantlinjer](./border-radius.html)
   * [Många egenskaper på kantlinjen](./border-all.html)
