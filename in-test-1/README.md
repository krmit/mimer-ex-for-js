# Detta är ett exempel

Detta är ett test på en övning, för att se hur denna typ av inlämning fungerar och för att du ska lära dig hur dessa övningar kommer gå till.

  * Du ska fixa till så denna lila sida ser bra ut.
  * Se till att allt får en stil. Eller att helheten ser snygg ut.
    * Du ska skriva alla din ändringar till filen my.css. 
    * Du ska inte ändra på html koden på något sätt.
    * Vill du lämna in fler stiler gör det som helt separata projekt.
    * Om du använder dig av en bild i din stil, lägg den i mappen "image".
  * När du är klara packar du ihop denna fil.
  * Du går in på [inlämningssidan](https://htsit.se/i/form/web-test-1).
  * Laddar upp din fil där.
  * Detta ska göras senast 10 minuter innan lektionen slutar. Oavsett om du tycker du är klar eller inte.   
