# Övning med bilder och listor

Detta är en övning i olika css egenskaper som bilder och listor har.

## Regler

  * Du ska skriva alla din ändringar till filen my.css. 
  * Du ska inte ändra på html koden på något sätt.
  * Vill du lämna in fler stiler så ge dem ett annat namn på css filen och ange namnet när du lämnar in dem.
  * Om du använder dig av en bild i din stil ladda upp den också, men se till att anvädna rätt namn. Bildenm kopmmer hamn i "image" mappen.
  * Du ska läman in senast 10 minuter innan lektionen slutar. Oavsett om du tycker du är klar eller inte.
     
  * När du ska lämna in något:
    * Du går in på [inlämningssidan](https://htsit.se/i/form/in-list-and-images).
    * Laddar upp din fil där.
    * Skriv in ditt alias.
    * Skriv ett namn om du lämnar in en bild eller fler css filer.

  
## Uppgiften
  
  * Se till att använda så många olika CSS attribute på listorna som möjligt.
  * Se till att alla bilder visas på olika sätt.
  * Du kan också fixa till så denna lilla sida ser bra ut.
