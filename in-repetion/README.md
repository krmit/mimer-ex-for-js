# Övning om CSS så här långt

Detta är en övning i olika css egenskaper som vi lärt oss så här långt

## Regler

  * Du ska skriva alla din ändringar till filen my.css. 
  * Du ska inte ändra på html koden på något sätt.
  * Du ska heller inte ändra på css koden som är i base.css.
  * Vill du lämna in fler stiler så ge dem ett annat namn på css 
  filen och ange namnet när du lämnar in dem. 
  * Om du använder dig av en bild i din stil ladda upp den också, men 
  se till att anvädna rätt namn. Bildenm kopmmer hamna i "image" mappen. 
  * Du ska läman in innan lektionen slutar. Oavsett om du tycker du 
  är klar eller inte.
  * Visa frågor kanske du tycker är försvåra. Hoppa då bara över 
  dem och försök göra dem när dufår tid.
  * Alla hjälpmedel är tillåtna. Men undvik att komunicera med andra 
  medan du löser uppgifterna.
     
  * När du ska lämna in något:
    * Du går in på 
    [inlämningssidan](https://htsit.se/i/form/in-repetion).
    * Laddar upp din fil där.
    * Skriv in ditt alias.
    * Skriv ett namn om du lämnar in bilder eller flera css filer.
    * Tryck på skicka och titta på bekräftelse sidan att du har 
    skickat in.

## Uppgiften
  
  * Gå till index.html och gör alla uppgifterna som är där. De är 
  i kommentarena till koden.
