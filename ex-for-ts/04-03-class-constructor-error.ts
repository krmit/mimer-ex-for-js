"use strict";

class A {
    a: number=1;
    b: string;
       
    constructor() {
		this.a = "2"; //  error TS2322: Type '"2"' is not assignable to type 'number'.
		this.b="Your number is: ";
	}
}

let a = new A();

console.log(a.b+a.a);
