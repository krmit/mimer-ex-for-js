"use strict";

interface MathOperator {
    (x: number, y: number): number;
}

let add : MathOperator;
let mult : MathOperator;

add = function (x:number, y:number):number {
    return x+y;
}

mult = function (x:number, y:number):number {
    return x*y;
}
