"use strict";


let add : (x: number, y: number) => number;

add = function (x:number, y:number):number {
    return x+y;
}

add = function (x:string, y:string):number { // error TS2322: Type '(x: string, y: string) => number' is not assignable to type '(x: number, y: number) => number'.
    return Number(x)*Number(y);
}
