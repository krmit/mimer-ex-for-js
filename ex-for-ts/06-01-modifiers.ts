"use strict";

class A {
    private a: number=1;
    private msg: string;
       
    constructor(a:number) {
		this.a = a;
		this.msg="Your number is: ";
	}
	
	label():string {
	   return String(a.msg+a.a);
	}
	
	print():void {
	   console.log(this.label());
	}
	
	setMSG(msg:string):void {
	   this.msg=msg;
	}
}

let a = new A(4);

a.setMSG("Annas number is: ")
a.print();
