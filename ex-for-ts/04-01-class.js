"use strict";
var A = /** @class */ (function () {
    function A() {
        this.a = 0;
        this.b = "hello";
    }
    return A;
}());
var a = new A();
console.log(a.b);
