"use strict";

class A {
    a: number = 0;
    b: string = "hello";
}

let a = new A();

console.log(a.b);
