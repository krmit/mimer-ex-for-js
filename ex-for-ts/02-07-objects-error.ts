"use strict"

let sprite  = {x:0,y:1};

sprite.z = 3; // error TS2339: Property 'z' does not exist on type '{ x: number; y: number; }'.
   
console.log(sprite.x+sprite.y+sprite.z); // error TS2339: Property 'z' does not exist on type '{ x: number; y: number; }'.

