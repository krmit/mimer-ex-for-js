"use strict";

function add(x:number, y:number):number {
    return x+y;
}

let a = add(1,2);
let b = add("3",4); // error TS2345: Argument of type '"3"' is not assignable to parameter of type 'number'.

console.log(String(a+b));

